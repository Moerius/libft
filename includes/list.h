/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 17:03:55 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:14:06 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_avl
{
	void			*content;
	size_t			content_size;
	struct s_avl	*left;
	struct s_avl	*right;
}					t_avl;

t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*dem)(void *, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list				*ft_lstdup(t_list *lst);
size_t				ft_lstlen(t_list **head);
void				ft_lstpush(t_list **alst, t_list *new);
void				ft_lstprint(t_list **head);
t_list				**ft_tab_to_list(char **tab);

t_avl				*ft_avlnew(void const *content, size_t content_size);
t_avl				*ft_avladd(t_avl *root, t_avl *node, \
		int (*cmp)(void *, void *));
void				ft_avliter_inorder(t_avl *head, void (*f)(t_avl *node));
void				ft_avliter_inorderrev(t_avl *head, void (*f)(t_avl *node));
void				ft_avldel(t_avl **head, void (*del)(void *, size_t));
int					ft_avlheight(t_avl *node);

#endif
