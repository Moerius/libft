/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 17:05:10 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:18:16 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STR_H
# define STR_H

typedef struct		s_fd
{
	int				fd;
	char			*text;
	struct s_fd		*next;
	struct s_fd		*begin_list;
}					t_fd;

char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strfreejoin(char *s1, char *s2, int inf);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
char				**ft_strbsplit(char const *s);
char				*ft_deln(char const *buf);
size_t				ft_strlen_utf8(const wchar_t *s);
int					get_next_line(int const fd, char **line);
size_t				ft_nstrlen(const char *str, size_t maxlen);
size_t				ft_strclen(char *s, char c);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strdup(const char *s);
char				*ft_strcpy(char *dest, const char *src);
char				*ft_strncpy(char *dest, const char *src, size_t n);
char				*ft_strcat(char *dest, const char *src);
char				*ft_strncat(char *dest, const char *src, size_t n);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strrev(char *str);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t len);
size_t				ft_strlcat(char *dest, const char *src, size_t n);
size_t				ft_strlen(char const *s);
char				**ft_strdup_tab(char **tab);
char				*ft_strremplace(char *s1, size_t start,
							size_t length, char *s2);
char				**ft_list_to_tab(t_list **head, size_t size_tab);
#endif
