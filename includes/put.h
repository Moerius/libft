/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 17:03:34 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:06:35 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUT_H
# define PUT_H

void				ft_print_rep(int c, size_t repeat);
void				*ft_print_memory(void *addr, unsigned int size);
int					ft_putnbrhex(unsigned long long nb, unsigned int len,\
		int maj);
int					ft_putnbroct(unsigned long long nb, unsigned int len);
int					ft_putnbrdec(unsigned int nb, unsigned int len);
int					ft_putnbrbin(unsigned long long nb);
size_t				ft_putchar_utf8(const unsigned int cp);
size_t				ft_putstr_utf8(const wchar_t *s);
int					ft_putnbr_base(size_t nb, char *base);
void				ft_putnstr(const char *str, size_t str_len);
void				ft_putnstr_fd(const char *str, size_t str_len, int fd);
void				ft_putwchar(wchar_t chr);
void				ft_putwchar_fd(wchar_t chr, int fd);
void				ft_putnwstr(const wchar_t *str, size_t len);
void				ft_putnbrendl(int n);
size_t				ft_putstr(char const *s);
size_t				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr(int n);
void				ft_putnbr_fd(int n, int fd);
size_t				ft_putchar(char c);
size_t				ft_putchar_fd(char c, int fd);
size_t				ft_putendl(char const *str);
size_t				ft_putendl_fd(char const *str, int fd);
void				ft_puttab(char **tab);
#endif
