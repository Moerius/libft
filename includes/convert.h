/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 17:03:44 by atheveno          #+#    #+#             */
/*   Updated: 2016/06/22 13:58:24 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONVERT_H
# define CONVERT_H

int					ft_atoi(const char *s);
long				ft_atol(const char *str);
char				*ft_itoa(int c);
char				*ft_ftoa(double n, size_t precision);
#endif
