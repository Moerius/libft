/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 17:03:06 by atheveno          #+#    #+#             */
/*   Updated: 2016/06/22 13:58:04 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

int					ft_printf(const char *format, ...);

typedef struct		s_arg
{
	int				force_prefix : 1;
	int				pad_zeroes : 1;
	int				right_pad : 1;
	int				force_sign : 1;
	int				blank_sign : 1;
	int				got_width : 1;
	int				got_precision : 1;
	unsigned int	width;
	unsigned int	precision;
	enum {
		none,
		hh,
		h,
		l,
		ll,
		j,
		z
	}				length;
}					t_arg;

ssize_t				handle_escape(char **format, va_list *args, t_arg *arg);
ssize_t				handle_str(char **format, va_list *args, t_arg *arg);
ssize_t				handle_wstr(char **format, va_list *args, t_arg *arg);
ssize_t				handle_ptr(char **format, va_list *args, t_arg *arg);
ssize_t				handle_int(char **format, va_list *args, t_arg *arg);
ssize_t				handle_long(char **format, va_list *args, t_arg *arg);
ssize_t				handle_octal(char **format, va_list *args, t_arg *arg);
ssize_t				handle_unsigned(char **format, va_list *args, t_arg *arg);
ssize_t				handle_hex(char **format, va_list *args, t_arg *arg);
ssize_t				handle_char(char **format, va_list *args, t_arg *arg);
ssize_t				handle_wchar(char **format, va_list *args, t_arg *arg);
ssize_t				handle_null(char **format, va_list *args, t_arg *arg);
ssize_t				handle_binary(char **format, va_list *args, t_arg *arg);
ssize_t				handle_float(char **format, va_list *args, t_arg *arg);
ssize_t				handle_charswritten(char **format, va_list *a, t_arg *arg);
ssize_t				handle_uint(uintmax_t nbr, t_arg *arg, char *b, char *p);
typedef ssize_t		(*t_handler)(char**, va_list*, t_arg*);
t_handler			get_handler(char c);
char				*parse_flags(char **format, t_arg *arg);
char				*parse_width(char **format, va_list *list, t_arg *arg);
char				*parse_precision(char **format, va_list *list, t_arg *arg);
char				*parse_length(char **format, t_arg *arg);
uintmax_t			get_unsigned_from_length(va_list *args, t_arg *arg);
void				width_pad(int nbrstrlen, int width, char padwith,
		t_arg *arg);
unsigned int		nbrlen(uintmax_t nbr, char *base);
unsigned int		calc_nbrstrlen(uintmax_t nbr, char *base, char *p,
		t_arg *arg);
ssize_t				nbrforceprefix(uintmax_t n, char *b, t_arg *a, char *p);
#endif
