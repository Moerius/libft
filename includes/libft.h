/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 13:37:27 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/08 16:41:40 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <unistd.h>
# include <stdlib.h>
# include <wchar.h>
# include <inttypes.h>
# include <stdarg.h>
# include <stdint.h>
# include <stddef.h>
# include <sys/types.h>

# include "check.h"
# include "colors.h"
# include "convert.h"
# include "ft_printf.h"
# include "list.h"
# include "mem.h"
# include "put.h"
# include "sorting.h"
# include "str.h"
# include "utils.h"

# define BUFFER_SIZE 32

#endif
