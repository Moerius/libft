# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/28 17:18:20 by atheveno          #+#    #+#              #
#    Updated: 2016/07/20 17:14:29 by atheveno         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY : $(NAME) help all clean fclean re save

NAME = libft.a 
SPATH = srcs
OPATH = bin
SRC = 
OBJ = 	$(addprefix $(OPATH)/, $(SRC:.c=.o))
EXT = 	./includes/$(NAME:.a=.h)
EXT +=	Makefile
NOW := $(shell date +"%c" | tr ' :' '_')

# CHECK
#
SRC +=	ft_isalnum.c \
		ft_isalpha.c \
		ft_isascii.c \
		ft_isblank.c \
		ft_isdigit.c \
		ft_isprint.c \
		ft_isspace.c \
		ft_max.c

# CONVERT
#
SRC += ft_atoi.c \
	   ft_atol.c \
	   ft_ftoa.c \
	   ft_itoa.c

# FT_PRINTF
#
SRC += ft_printf.c \
	   parse_handlers.c \
	   generic_handle_unsigned.c \
	   get_handler_arr.c \
	   handle_binary.c \
	   handle_char.c \
	   handle_charswritten.c \
	   handle_escape.c \
	   handle_float.c \
	   handle_hex.c \
	   handle_int.c \
	   handle_long.c \
	   handle_null.c \
	   handle_octal.c \
	   handle_ptr.c \
	   handle_str.c \
	   handle_unsigned.c \
	   handle_wchar.c \
	   handle_wstr.c \
	   calc_nbrstrlen.c \
	   get_unsigned_from_length.c \
	   nbrforceprefix.c \
	   nbrlen.c \
	   width_pad.c

# LIST
#
SRC += ft_lstdelone.c \
	   ft_lstnew.c \
	   ft_lstadd.c \
	   ft_lstiter.c \
	   ft_lstlast.c \
	   ft_lstlen.c \
	   ft_lstmap.c \
	   ft_lstdup.c \
	   ft_lstprint.c \
	   ft_lstpush.c \
	   ft_tab_to_list.c \
	   ft_avlnew.c \
	   ft_avlheight.c \
	   ft_avladd.c \
	   ft_avldel.c \
	   ft_avliter_inorder.c \
	   ft_avliter_inorderrev.c

# MEM
#
SRC += ft_bzero.c \
	   ft_freetab.c \
	   ft_memalloc.c \
	   ft_memccpy.c \
	   ft_memchr.c \
	   ft_memcmp.c \
	   ft_memcpy.c \
	   ft_memdel.c \
	   ft_memmove.c \
	   ft_memset.c \
	   ft_realloc_tab.c

# PUT
#
SRC += ft_print_memory.c \
	   ft_print_rep.c \
	   ft_putchar.c \
	   ft_putchar_fd.c \
	   ft_putchar_utf8.c \
	   ft_putendl.c \
	   ft_putendl_fd.c \
	   ft_putnbr.c \
	   ft_putnbr_base.c \
	   ft_putnbr_fd.c \
	   ft_putnbrbin.c \
	   ft_putnbrdec.c \
	   ft_putnbrendl.c \
	   ft_putnbrendl_fd.c \
	   ft_putnbrhex.c \
	   ft_putnbroct.c \
	   ft_putnstr.c \
	   ft_putnstr_fd.c \
	   ft_putnwstr.c \
	   ft_putstr.c \
	   ft_putstr_fd.c \
	   ft_putstr_utf8.c \
	   ft_puttab.c \
	   ft_putwchar.c \
	   ft_putwchar_fd.c

# SORTING
#
SRC += ft_qsort.c

# STR
#
SRC += ft_nstrlen.c \
	   ft_strcat.c \
	   ft_strchr.c \
	   ft_strclen.c \
	   ft_strclr.c \
	   ft_strcmp.c \
	   ft_strcpy.c \
	   ft_strdel.c \
	   ft_strdup.c \
	   ft_strdup_tab.c \
	   ft_strequ.c \
	   ft_striter.c \
	   ft_strjoin.c \
	   ft_strfreejoin.c \
	   ft_strlcat.c \
	   ft_strlen.c \
	   ft_strlen_utf8.c \
	   ft_strmap.c \
	   ft_strmapi.c \
	   ft_strncat.c \
	   ft_strncmp.c \
	   ft_strncpy.c \
	   ft_strnequ.c \
	   ft_strnew.c \
	   ft_strnstr.c \
	   ft_strrchr.c \
	   ft_strrev.c \
	   ft_strremplace.c \
	   ft_strsplit.c \
	   ft_strbsplit.c \
	   ft_strstr.c \
	   ft_strsub.c \
	   ft_strtrim.c \
	   ft_tolower.c \
	   ft_toupper.c \
	   get_next_line.c

# UTILS
#
SRC += ft_swap.c \
	   ft_tab_size.c

vpath %.c \
	srcs/check: \
	srcs/convert: \
	srcs/list: \
	srcs/math: \
	srcs/mem: \
	srcs/put: \
	srcs/str: \
	srcs/utils: \
	srcs/ft_printf/src: \
	srcs/ft_printf/src/utils: \
	srcs/ft_printf/src/handlers: \
	srcs/sorting


all: $(NAME)

help:
	@echo "     clean           to clean the object files."
	@echo "     fclean          to clean all objects files and executable."
	@echo "     re              to remake the librairy"
	@echo "     save            to git add --all and commit as the time $(NOW)"

$(NAME): $(OBJ) $(EXT)
	@echo 
	@echo "-> Creating $(NAME)..."
	@ar rc $(NAME) $(OBJ)
	@echo "-> Optimising $(NAME)..."
	@ranlib $(NAME)
	@echo "-> Done !"

bin/%.o: %.c $(EXT) 
	@mkdir -p bin 
	@gcc -Werror -Wextra -Wall -Iincludes/ -c $< -o $@

clean: 
	@/bin/rm -rf bin/

fclean: clean
	@/bin/rm -rf $(NAME)

re: fclean all

save:
	@git add --all
	@git commit -m 'saving $(NOW)'
	@echo "$(YELLOW)all files added and commited$(WHITE)"
