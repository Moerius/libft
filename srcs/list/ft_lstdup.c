/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/30 09:09:46 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/08 11:50:00 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstdup(t_list *lst)
{
	t_list	*dup;
	t_list	*tmp;
	int		i;

	dup = NULL;
	tmp = NULL;
	i = 0;
	while (lst)
	{
		if (i == 0)
		{
			dup = ft_lstnew(lst->content, lst->content_size);
			tmp = dup;
		}
		else
			ft_lstpush(&dup, ft_lstnew(lst->content, lst->content_size));
		lst = lst->next;
		i++;
	}
	return (tmp);
}
