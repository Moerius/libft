/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avlnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 17:36:01 by atheveno          #+#    #+#             */
/*   Updated: 2016/03/24 22:30:13 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_avl	*ft_avlnew(void const *content, size_t content_size)
{
	t_avl	*node;

	if (!(node = (t_avl *)malloc(sizeof(t_avl))))
		return (NULL);
	if (content)
	{
		if (!(node->content = (void *)malloc(content_size)))
			return (NULL);
		node->content = ft_memmove(node->content, content, content_size);
	}
	else
		node->content = NULL;
	node->content_size = !content ? 0 : content_size;
	node->left = NULL;
	node->right = NULL;
	return (node);
}
