/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstprint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 15:50:09 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:12:38 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstprint(t_list **head)
{
	t_list	*cur;

	cur = *head;
	if (!*head)
		return ;
	while (cur)
	{
		if (!cur->next)
			break ;
		if (cur->content)
			ft_putendl(cur->content);
		cur = cur->next;
	}
	if (cur->content)
		ft_putendl(cur->content);
}
