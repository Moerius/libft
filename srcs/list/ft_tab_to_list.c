/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tab_to_list.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 17:18:07 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:18:09 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		**ft_tab_to_list(char **tab)
{
	t_list	**head;
	t_list	*lst;
	size_t	n;

	n = 1;
	if (!(head = (t_list **)malloc(sizeof(t_list *))))
		return (NULL);
	lst = ft_lstnew(tab[0], ft_strlen(tab[0]));
	*head = lst;
	while (tab[n])
	{
		lst->next = ft_lstnew(tab[n], ft_strlen(tab[n]));
		lst = lst->next;
		n++;
	}
	return (head);
}
