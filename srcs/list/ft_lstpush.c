/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpush.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/30 09:14:48 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/08 14:28:19 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstpush(t_list **alst, t_list *new)
{
	t_list *index;

	index = *alst;
	if (!index)
		*alst = new;
	else
	{
		while (index->next != NULL)
			index = index->next;
		index->next = new;
		new->next = NULL;
	}
}
