/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avldel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:20:20 by atheveno          #+#    #+#             */
/*   Updated: 2016/03/24 18:45:12 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_avldel(t_avl **head, void (*del)(void *, size_t))
{
	t_avl	*tmp;

	tmp = *head;
	if (tmp)
	{
		ft_avldel(&tmp->left, del);
		ft_avldel(&tmp->right, del);
		(*del)(tmp->content, tmp->content_size);
		free(tmp);
		*head = NULL;
	}
}
