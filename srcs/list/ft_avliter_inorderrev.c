/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avliter_inorderrev.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:47:45 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/17 20:56:47 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_avliter_inorderrev(t_avl *head, void (*f)(t_avl *node))
{
	if (!head)
		return ;
	ft_avliter_inorderrev(head->right, f);
	(*f)(head);
	ft_avliter_inorderrev(head->left, f);
}
