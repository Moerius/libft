/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avlheight.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 17:44:24 by atheveno          #+#    #+#             */
/*   Updated: 2016/03/24 18:01:24 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_avlheight(t_avl *node)
{
	int height;
	int right_height;
	int	left_height;
	int	max;

	height = 0;
	if (node != NULL)
	{
		left_height = ft_avlheight(node->left);
		right_height = ft_avlheight(node->right);
		max = (left_height > right_height) ? left_height : right_height;
		height = 1 + max;
	}
	return (height);
}
