/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_null.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 18:17:16 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/26 23:37:36 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t	handle_null(char **format, va_list *args, t_arg *arg)
{
	(void)args;
	if (arg->got_width && !arg->right_pad)
		width_pad(1, arg->width, arg->pad_zeroes ? '0' : ' ', arg);
	ft_putchar(**format);
	if (arg->got_width && arg->right_pad)
		width_pad(1, arg->width, ' ', arg);
	return (arg->got_width ? ft_max(arg->width, 1) : 1);
}
