/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_utf8.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/11 17:55:22 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/29 01:07:04 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

size_t	ft_putstr_utf8(const wchar_t *s)
{
	size_t i;

	i = -1;
	while (++i <= ft_strlen_utf8(s))
		ft_putchar_utf8((unsigned int)s[i]);
	return (ft_strlen_utf8(s));
}
