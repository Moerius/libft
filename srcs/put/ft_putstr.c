/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/06 16:06:20 by atheveno          #+#    #+#             */
/*   Updated: 2016/06/17 17:08:25 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

size_t	ft_putstr(char const *s)
{
	if (s)
		write(1, s, ft_strlen(s));
	else
		return (0);
	return (ft_strlen(s));
}
