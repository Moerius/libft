/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/27 13:45:52 by atheveno          #+#    #+#             */
/*   Updated: 2016/02/16 20:02:27 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

int		ft_putnbr_base(size_t nb, char *base)
{
	size_t const b = ft_strlen(base);

	return ((nb >= b ? ft_putnbr_base(nb / b, base) : 0) \
		+ ft_putchar(base[nb % b]));
}
