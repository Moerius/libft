/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strfreejoin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 15:30:40 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 15:30:47 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strfreejoin(char *s1, char *s2, int inf)
{
	char	*tmp;

	tmp = ft_strjoin(s1, s2);
	if (s1 && (inf == 3 || inf == 2))
	{
		free(s1);
		s1 = NULL;
	}
	if (s2 && (inf == 3 || inf == 1))
	{
		free(s2);
		s2 = NULL;
	}
	return (tmp);
}
