/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/20 08:31:53 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:32:29 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strrchr(const char *s, int c)
{
	const char *tmp;

	if (!s)
		return ((char*)s);
	tmp = s + (ft_strlen(s) ? ft_strlen(s) : 0);
	while (tmp && tmp >= s)
	{
		if (*tmp == (char)c)
			return ((char*)tmp);
		tmp--;
	}
	return (NULL);
}
