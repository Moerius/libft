/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/20 08:49:19 by atheveno          #+#    #+#             */
/*   Updated: 2016/06/21 03:29:49 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strdup(const char *s)
{
	char	*tmp;

	tmp = (char *)malloc(sizeof(char) * ft_strlen((char *)(s)) + 1);
	if (tmp == NULL)
		return (NULL);
	tmp = ft_strcpy(tmp, s);
	return (tmp);
}
