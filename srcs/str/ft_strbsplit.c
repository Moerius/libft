/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strbsplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 12:37:13 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/07 12:41:08 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

static int		ft_size(char const *s)
{
	int		i;
	int		size;

	i = 0;
	size = 0;
	while (s[i] != '\0')
	{
		while (ft_isblank(s[i]))
			i++;
		if (!ft_isblank(s[i]))
			if (ft_isblank(s[i + 1]) || s[i + 1] == '\0')
				size++;
		i++;
	}
	return (size);
}

static size_t	ft_length(char const *s, int i)
{
	size_t	len;

	len = 0;
	while (!ft_isblank(s[i]) && s[i] != '\0')
	{
		len++;
		i++;
	}
	return (len);
}

char			**ft_strbsplit(char const *s)
{
	char	**res;
	int		i;
	int		j;

	i = 0;
	j = 0;
	res = (char **)malloc(sizeof(char *) * (ft_size(s) + 1));
	if (res == NULL)
		return (res);
	while (s[i] != '\0')
	{
		if (!ft_isblank(s[i]))
		{
			res[j] = ft_strsub(s, i, ft_length(s, i));
			j++;
			while (!ft_isblank(s[i]) && s[i] != '\0')
				i++;
		}
		else
			i++;
	}
	i = 0;
	res[j] = NULL;
	return (res);
}
