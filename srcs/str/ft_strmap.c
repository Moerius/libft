/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:12:34 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:30:27 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char			*tmp;
	unsigned int	index;

	index = 0;
	tmp = ft_strnew(ft_strlen(s));
	if (tmp == NULL || s == NULL)
		return (NULL);
	while (s[index])
	{
		tmp[index] = f(s[index]);
		index++;
	}
	tmp[index] = '\0';
	return (tmp);
}
