/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup_tab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/06 17:59:55 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:09:40 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strdup_tab(char **tab)
{
	char	**new;
	size_t	size_tab;
	size_t	i;

	size_tab = ft_tab_size(tab);
	new = (char **)malloc(sizeof(char *) * (size_tab + 1));
	if (new == NULL)
		return (NULL);
	i = 0;
	if (tab)
	{
		while (i < size_tab)
		{
			new[i] = ft_strdup(tab[i]);
			if (new[i] == NULL)
				ft_freetab(new);
			i++;
		}
	}
	new[i] = NULL;
	return (new);
}
