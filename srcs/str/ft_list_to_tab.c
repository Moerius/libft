/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 17:17:01 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/20 17:17:20 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_list_to_tab(t_list **head, size_t size_tab)
{
	t_list	*cur;
	char	**tab;
	size_t	n;

	n = 0;
	cur = *head;
	tab = (char **)malloc(sizeof(char *) * (size_tab + 1));
	if (!tab)
		return (NULL);
	while (cur)
	{
		if (cur->content == NULL)
			break ;
		tab[n] = ft_strdup(cur->content);
		cur = cur->next;
		n++;
	}
	tab[n] = NULL;
	return (tab);
}
