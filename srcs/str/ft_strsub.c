/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:58:16 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/13 17:30:15 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*tmp;
	size_t	index;

	index = 0;
	tmp = (char *)malloc(sizeof(*s) * len + 1);
	if (tmp == NULL)
		return (NULL);
	if (s == NULL)
	{
		free(tmp);
		return (NULL);
	}
	while (index < len)
	{
		tmp[index] = s[start];
		index++;
		start++;
	}
	tmp[index] = '\0';
	return (tmp);
}
