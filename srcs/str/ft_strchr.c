/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/20 08:29:24 by atheveno          #+#    #+#             */
/*   Updated: 2016/03/17 15:45:49 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strchr(const char *s, int c)
{
	size_t index;
	size_t length;

	index = 0;
	length = ft_strlen(s);
	while (index <= length)
	{
		if (s[index] == (char)c)
			return ((char*)(s + index));
		index++;
	}
	return (NULL);
}
