/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/20 09:18:26 by atheveno          #+#    #+#             */
/*   Updated: 2016/01/09 14:32:18 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t len)
{
	size_t	index;
	size_t	index2;

	index = 0;
	index2 = 0;
	if (len < ft_strlen((char *)s2))
		return (NULL);
	while (index < len && s1[index] != '\0' && s2[index2] != '\0')
	{
		if (s1[index] == s2[index2])
			index2++;
		else
		{
			index -= index2;
			index2 = 0;
		}
		index++;
	}
	if (s2[index2] == '\0')
		return ((char *)&s1[index - index2]);
	return (NULL);
}
