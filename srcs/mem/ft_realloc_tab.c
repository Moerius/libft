/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/30 10:53:28 by atheveno          #+#    #+#             */
/*   Updated: 2016/06/13 13:46:14 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_realloc_tab(char ***tab, size_t n)
{
	char	**new;
	int		size_tab;
	int		i;

	size_tab = 0;
	if (*tab)
		while ((*tab)[size_tab] != 0)
			size_tab++;
	new = (char **)malloc(sizeof(char *) * (size_tab + n + 1));
	new[size_tab + n] = NULL;
	i = -1;
	if (*tab)
	{
		while (++i < size_tab)
			new[i] = (*tab)[i];
		free(*tab);
	}
	*tab = new;
}
