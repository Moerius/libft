/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_qsort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 18:54:00 by atheveno          #+#    #+#             */
/*   Updated: 2016/03/24 19:01:35 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_quicksort(char **items, int left, int right)
{
	int		i_j[2];
	char	*x;
	char	*tmp;

	i_j[0] = left;
	i_j[1] = right;
	x = items[(i_j[0] + i_j[1]) / 2];
	while (i_j[0] <= i_j[1])
	{
		while (ft_strcmp(items[i_j[0]], x) < 0 && i_j[0] < right)
			i_j[0]++;
		while (ft_strcmp(items[i_j[1]], x) > 0 && i_j[1] > left)
			i_j[1]--;
		if (i_j[0] <= i_j[1])
		{
			tmp = ft_strdup(items[i_j[0]]);
			ft_strcpy(items[i_j[0]++], items[i_j[1]]);
			ft_strcpy(items[i_j[1]--], tmp);
			free(tmp);
		}
	}
	if (left < i_j[1])
		ft_quicksort(items, left, i_j[1]);
	if (i_j[0] < right)
		ft_quicksort(items, i_j[0], right);
}
