/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atheveno <atheveno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/17 20:59:03 by atheveno          #+#    #+#             */
/*   Updated: 2016/07/17 21:33:23 by atheveno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "libft.h"

typedef struct noeud
{
	char			info;
	struct noeud	*fils;
	struct noeud	*frere;
}				NOEUD, *PTR;

static struct
{
	char	t[80];
	int		n;
}				pile;

PTR		noeud(char info, PTR fils, PTR frere)
{
	PTR p;

	p = malloc(sizeof(NOEUD));
	if (p != NULL)
	{
		p->info = info;
		p->fils = fils;
		p->frere = frere;
	}
	return (p);
}

int		rechinsertion(char *mot, PTR ancetre)
{
	PTR pr;
	PTR pc;
	int i;

	for (i=0; ;i++)
	{
		pr = NULL;
		pc = ancetre->fils;
		while (pc != NULL && pc->info < mot[i])
		{
			pr = pc;
			pc = pc->frere;
		}
		if (pc != NULL && pc->info == mot[i])
		{
			if (mot[i] == '\0')
				return (1); /* le mot existait */
			ancetre = pc;
		}
		else
		{
			pc = noeud(mot[i], NULL, pc);
			if (pr != NULL)
				pr->frere = pc;
			else
				ancetre->fils = pc;
			while (mot[i] != '\0')
			{
				pc->fils = noeud(mot[++i], NULL, NULL);
				pc = pc->fils;
			}
			return (0); /* le mot est nouveau */
		}
	}
	return (0);
}

void	parcours(PTR arbre)
{
	PTR p;
	pile.t[pile.n++] = arbre->info;
	if (arbre->info == '\0')
		printf("%s\n", pile.t + 1);
	else
		for (p = arbre->fils; p != NULL; p = p->frere)
			parcours(p);
	pile.n--;
}

int		main(void)
{
	char mot[80];
	int i;
	PTR racine = noeud('!', NULL, NULL);
	while (gets(mot) != NULL)
		printf("le mot %s est %s\n", mot,
				rechinsertion(mot, racine) ? "ancien" : "nouveau");
	pile.n = 0;
	parcours(racine);
}
